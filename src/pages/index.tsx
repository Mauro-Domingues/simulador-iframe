import React, { useCallback, useEffect, useRef, useState } from 'react';
import {
  FaBolt,
  FaDownload,
  FaEnvelope,
  FaFileUpload,
  FaIndustry,
  FaPhoneAlt,
  FaPrint,
  FaRulerCombined,
  FaSolarPanel,
  FaTree,
  FaWhatsapp,
} from 'react-icons/fa';
import * as Yup from 'yup';

import { FormHandles, useField } from '@unform/core';
import { Form } from '@unform/web';

import { getCookie, setCookie } from 'cookies-next';
import { GetServerSideProps, NextPage } from 'next';
import Image from 'next/image';
import Link from 'next/link';

import { useToast } from '@/hooks/toast';
import api from '@/services/api';
import getValidationErrors from '@/utils/getvalidationsErrors';
import phoneHandler from '@/utils/phoneHandler';

import { IOptions } from '@/interfaces/components/form/Select';

import { simulationSchema } from '@/schema/simulation';

import Card from '@/components/Card';
import Container from '@/components/Container';
import Button from '@/components/Form/Button';
import Input from '@/components/Form/Input';
import ImageInput from '@/components/Form/InputFile';
import InputMask from '@/components/Form/InputMask';
import Select from '@/components/Form/Select';
import ButtonPreview from '@/components/Previews/Button';

import { Content, HeaderCard } from '@/styles/pages/Simulator';

import IconMoney from '../../public/assets/icons/icon-money.svg';
import IconPanelSolar from '../../public/assets/icons/icon-panel-solar.svg';
import IconSustainableWorld from '../../public/assets/icons/icon-sustainable-world.svg';

interface IProps {
  buttonBG?: string;
  buttonBGTransparent?: boolean;
  buttonBorderColor?: string;
  buttonBorderTransparent?: boolean;
  buttonTextColor?: string;
  radiusButton?: string;
  brilhoButton?: boolean;

  inputBG?: string;
  inputBGTransparent?: boolean;
  inputBorderColor?: string;
  inputBorderTransparent?: boolean;
  inputTextColor?: string;

  inputRadius?: string;
  inputBrilho?: boolean;

  primaryColor?: string;
  secondaryColor?: string;
  tertiaryColor?: string;
  titleColor?: string;
}

interface ICompany {
  logo?: string;
  companyName?: string;
  email?: string;
  phone?: string;
  whatsapp?: string;
}

export interface IUser {
  id: string;
  avatar: string;
  birthday: string;
  cpf: string;
  email: string;
  gender: string;
  lastname: string;
  name: string;
  permissions: [];
  phone: null;
  roles: [string];
  slug: string;
  plan: any;
  clientData: any;
  clientSettings: any;
}

interface ISimulateData {
  name: string;
  email: string;
  phone: string;
  state_id: string;
  city_id: string;
  segment_id: string;
  value: number;
}

const Simulator: NextPage = () => {
  const formRef = useRef<FormHandles>(null);
  const { addToast } = useToast();

  const [category, setCategory] = useState<'form' | 'result'>('form');
  const [maskPhone, setMaskPhone] = useState('+99 (99) 99999 - 9999');
  const [states, setStates] = useState(null);
  const [segments, setSegments] = useState(null);
  const [cities, setCities] = useState(null);
  const [simulation, setSimulation] = useState(null);
  const [loading, setLoading] = useState(false);
  const [company, setCompany] = useState<ICompany>({
    logo: '/assets/logo-solar.png',
    companyName: 'Aureos',
    email: 'aureos@risestudio.com.br',
    phone: '000000000000',
    whatsapp: '0000000000000',
  });
  const [props, setProps] = useState<IProps>({
    buttonBG: '#F07D18',
    buttonBGTransparent: false,
    buttonBorderColor: '#878787',
    buttonBorderTransparent: false,
    buttonTextColor: '#ffffff',
    radiusButton: '0',
    brilhoButton: false,

    inputBG: '#ffffff',
    inputBGTransparent: false,
    inputBorderColor: '#878787',
    inputBorderTransparent: true,
    inputTextColor: '#878787',
    inputRadius: '0',
    inputBrilho: true,

    primaryColor: '#1E4A8A',
    secondaryColor: '#F07D18',
    tertiaryColor: '#6CA92E',
    titleColor: '#009ADD',
  });

  const handleCities = useCallback(
    async state => {
      setLoading(false);
      try {
        formRef.current?.setErrors({});

        await api.get(`/cities?state_id=${state}`).then(response => {
          const mappedCities = response.data.data.map(city => {
            return {
              label: city.name,
              value: city.id,
            };
          });

          setCities(mappedCities);
        });
      } catch (err) {
        addToast({
          type: 'error',
          title: 'Erro ao buscar as cidades',
          description:
            err?.response?.data?.message ||
            'Não foi possível buscar as cidades',
        });
      }

      setLoading(true);
    },
    [addToast],
  );

  useEffect(() => {
    async function getData() {
      setLoading(false);
      try {
        const clientSubscription = await api.post(
          '/clients/authenticate?token=20a444145d072ba2db54dc21e897b8d08c6de37e4fe1a53b82ca279034fc687e',
        );

        setProps({
          buttonBG:
            clientSubscription.data.company_style.buttons.buttonBG || '#F07D18',
          buttonBGTransparent:
            clientSubscription.data.company_style.buttons
              .buttonBGTransparent === 'true',
          buttonBorderColor:
            clientSubscription.data.company_style.buttons.buttonBorderColor ||
            '#878787',
          buttonBorderTransparent:
            clientSubscription.data.company_style.buttons
              .buttonBorderTransparent === 'true',
          buttonTextColor:
            clientSubscription.data.company_style.buttons.buttonTextColor ||
            '#ffffff',
          radiusButton:
            clientSubscription.data.company_style.buttons.radiusButton || '0',
          brilhoButton:
            clientSubscription.data.company_style.buttons.brilhoButton ===
            'true',

          inputBG:
            clientSubscription.data.company_style.inputs.inputBG || '#ffffff',
          inputBGTransparent:
            clientSubscription.data.company_style.inputs.inputBGTransparent ===
            'true',
          inputBorderColor:
            clientSubscription.data.company_style.inputs.inputBorderColor ||
            '#878787',
          inputBorderTransparent:
            clientSubscription.data.company_style.inputs
              .inputBorderTransparent === 'true',
          inputTextColor:
            clientSubscription.data.company_style.inputs.inputTextColor ||
            '#878787',
          inputRadius:
            clientSubscription.data.company_style.inputs.inputRadius || '0',
          inputBrilho:
            clientSubscription.data.company_style.inputs.inputBrilho === 'true',

          primaryColor:
            clientSubscription.data.company_style.colors.primaryColor ||
            '#1E4A8A',
          secondaryColor:
            clientSubscription.data.company_style.colors.secondaryColor ||
            '#F07D18',
          tertiaryColor:
            clientSubscription.data.company_style.colors.tertiaryColor ||
            '#6CA92E',
          titleColor:
            clientSubscription.data.company_style.colors.titleColor ||
            '#009ADD',
        });

        setCompany({
          logo:
            clientSubscription.data.company_settings.logo ||
            '/assets/logo-solar.png',
          companyName:
            clientSubscription.data.company_settings.company || 'Aureos',
          email:
            clientSubscription.data.company_settings.email ||
            'aureos@risestudio.com.br',
          phone:
            clientSubscription.data.company_settings.phone || '000000000000',
          whatsapp:
            clientSubscription.data.company_settings.whatsapp ||
            '0000000000000',
        });

        const mappedStates = await api.get('/states').then(statesData =>
          statesData.data.data.map(state => {
            return {
              label: state.name,
              value: state.id,
            };
          }),
        );

        setStates(mappedStates);

        const mappedSegments = await api.get('/segments').then(segmentsData =>
          segmentsData.data.data.map(segment => {
            return {
              label: segment.name,
              value: segment.id,
            };
          }),
        );

        setSegments(mappedSegments);
      } catch (err) {
        addToast({
          type: 'error',
          title: 'Error',
          description:
            err?.response?.data?.message ||
            'Ocorreu um erro ao buscar os dados',
        });
      }
    }
    getData();
    setLoading(true);
  }, [addToast]);

  const handleCategory = useCallback(param => {
    setCategory(param);
  }, []);

  const handleSubmit = useCallback(
    async (data: ISimulateData) => {
      setLoading(false);
      try {
        formRef.current?.setErrors({});

        // eslint-disable-next-line no-param-reassign
        data.phone = data.phone.replace(/\D/g, '');

        await simulationSchema.validate(data, {
          abortEarly: false,
        });

        const simulationData = await api.post(
          '/simulation?token=20a444145d072ba2db54dc21e897b8d08c6de37e4fe1a53b82ca279034fc687e',
          data,
        );

        simulationData.data.economyAnnual =
          simulationData.data.economyAnnual.toLocaleString('pt-br', {
            style: 'currency',
            currency: 'BRL',
          });

        simulationData.data.createdAt = `${new Date(
          Date.parse(simulationData.data.createdAt) -
            (new Date().getTimezoneOffset() / 60) * 3600000,
        )
          .toLocaleTimeString()
          .slice(0, -3)} - ${new Date(
          Date.parse(simulationData.data.createdAt) - new Date().getUTCDate(),
        ).toLocaleDateString()}`;

        simulationData.data.billingValue = data.value;

        setSimulation(simulationData.data);

        setCategory('result');
      } catch (err) {
        if (err instanceof Yup.ValidationError) {
          const errors = getValidationErrors(err);
          formRef.current?.setErrors(errors);
        } else {
          addToast({
            type: 'error',
            title: 'Erro ao carregar',
            description:
              err?.response?.data?.message ||
              'Ocorreu um erro ao fazer a simulação, tente novamente',
          });
        }
      }
      setLoading(true);
    },
    [addToast],
  );

  return (
    <>
      <Container>
        <Content isActive={category} {...props}>
          <HeaderCard isActive={category} {...props}>
            <button
              type="button"
              disabled={category === 'form'}
              onClick={() => handleCategory('form')}
            >
              Formulário
            </button>
            <button
              type="button"
              disabled={category === 'result'}
              onClick={() => handleCategory('result')}
            >
              Resultado
            </button>
          </HeaderCard>

          <div className="cards">
            <Card
              borderColor="yellowBorder"
              backgroundColor="rgba(250, 250, 250, 0.8)"
            >
              <h2>Simulação {company.companyName}</h2>
              <p>
                Cras volutpat feugiat mauris curabitur. Consectetur turpis
                tristique feugiat leo felis morbi. Integer sollicitudin auctor
                et faucibus eget tellus. Mi sodales eu.
              </p>

              <Form ref={formRef} onSubmit={handleSubmit}>
                <Input
                  name="name"
                  label="Nome"
                  placeholder="Seu nome"
                  {...props}
                />
                <div className="row">
                  <Input
                    name="email"
                    label="Email"
                    placeholder="Seu Email"
                    {...props}
                  />
                  <InputMask
                    name="phone"
                    label="Telefone"
                    placeholder="Seu Telefone"
                    mask={maskPhone}
                    onBlur={e => {
                      if (e.target.value.replace('_', '').length === 20) {
                        setMaskPhone('+99 (99) 9999 - 9999');
                      }
                    }}
                    onChange={e => {
                      if (e.target.value.replace('_', '').length < 20) {
                        setMaskPhone('+99 (99) 99999 - 9999');
                      }
                    }}
                    onFocus={e => {
                      if (e.target.value.replace('_', '').length === 20) {
                        setMaskPhone('+99 (99) 9999 - 9999');
                      }
                    }}
                    {...props}
                  />
                </div>

                <div className="row">
                  <Select
                    name="state_id"
                    label="Estado"
                    placeholder="Seu Estado"
                    textNoOptions="Não encontrado"
                    options={states || []}
                    onChange={(options: IOptions) => {
                      handleCities(options.value);
                    }}
                    {...props}
                  />

                  <Select
                    name="city_id"
                    label="Cidade"
                    placeholder="Sua Cidade"
                    textNoOptions="Não encontrado"
                    options={cities || []}
                    {...props}
                  />
                </div>

                <div className="row">
                  <Select
                    name="segment_id"
                    label="Segmento"
                    placeholder="Seu Segmento"
                    textNoOptions="Não encontrado"
                    options={segments || []}
                    {...props}
                  />
                  <Input
                    name="value"
                    label="Valor da conta"
                    placeholder="Seu Valor da conta"
                    {...props}
                  />
                </div>

                <label htmlFor="image">Anexar conta de luz</label>
                <div className="input-file">
                  <ImageInput name="image" label="" />

                  <label htmlFor="image">
                    <FaFileUpload /> Arraste aqui ou{' '}
                    <strong>selecione o arquivo</strong>
                  </label>
                </div>

                <ButtonPreview {...props}>Enviar</ButtonPreview>
              </Form>
            </Card>

            <Card
              borderColor="orange"
              backgroundColor="rgba(250, 250, 250, 0.8)"
            >
              <div className="result-header">
                <div className="div-logo">
                  {company?.logo && (
                    <Image
                      src={company.logo}
                      alt="Preview"
                      height={100}
                      width={357}
                      objectFit="contain"
                    />
                  )}

                  <p>Simulação de economia</p>
                </div>

                <div className="div-date">
                  <p>
                    {simulation?.createdAt ||
                      `${new Date().toLocaleTimeString().slice(0, -3)} -
                        ${new Date().toLocaleDateString()}`}
                  </p>
                </div>

                <div className="div-data">
                  <div className="buttons">
                    <button type="button">
                      <FaPrint /> <p>Imprimir</p>
                    </button>
                    <button type="button">
                      <FaDownload /> <p>Download</p>
                    </button>
                  </div>
                  <div className="ref">
                    <p>Referência:</p>
                    <span>22.0001-1</span>
                  </div>
                </div>
              </div>

              <div className="result">
                <hr className="line" />

                <h2>Resultados</h2>

                <p>
                  Cras volutpat feugiat mauris curabitur. Consectetur turpis
                  tristique feugiat leo felis morbi. Integer sollicitudin auctor
                  et faucibus eget tellus. Mi sodales eu.
                </p>

                <hr className="line" />

                <div className="row">
                  <aside />
                  <div className="applicant-data">
                    <div className="col">
                      <p>Solicitante </p> <span>{simulation?.name || ''}</span>
                    </div>
                    <div className="col">
                      <p>Cidade / Estado: </p>{' '}
                      <span>
                        {simulation?.city || ''} - {simulation?.state || ''}
                      </span>
                    </div>
                    <div className="col">
                      <p>Segmento: </p> <span>{simulation?.segment || ''}</span>
                    </div>
                  </div>
                </div>

                <div className="row">
                  <aside>
                    <IconMoney />
                  </aside>

                  <div className="col">
                    <h5>Sobre o projeto:</h5>
                    <p>
                      Tincidunt tortor, mattis luctus aliquam. Sem in in lacus,{' '}
                      <strong>
                        {' '}
                        conta no valor de {simulation?.billingValue || ''}{' '}
                      </strong>
                      quis. Phasellus laoreet consequat tortor tortor quisque
                      diam.
                    </p>
                  </div>
                </div>

                <div className="row">
                  <aside>
                    <hr />
                  </aside>
                  <div className="div-content">
                    <div className="card-value">
                      <p>
                        Ao longo de um ano,
                        <strong> você vai economizar:</strong>
                      </p>
                      <h2>{simulation?.economyAnnual || 'R$ 0,00'}</h2>
                      <p>
                        E terá o retorno do seu investimento em até
                        <strong>
                          {' '}
                          {Math.round(simulation?.payback.max) || '0'} anos
                        </strong>
                      </p>
                    </div>
                  </div>
                </div>

                <div className="row">
                  <aside>
                    <IconSustainableWorld />
                  </aside>

                  <div className="col">
                    <h5>Sustentabilidade</h5>
                    <p>
                      Enim fermentum consequat tortor vulputate sollicitudin et
                      libero, suspendisse. Enim purus vitae a nunc mattis sed
                      pharetra natoque.
                    </p>
                  </div>
                </div>

                <div className="row">
                  <aside>
                    <hr />
                  </aside>
                  <div className="div-content">
                    <div className="div-infos">
                      <div className="col">
                        <p>
                          <FaIndustry /> Redução de CO²
                        </p>
                        <h5>
                          {simulation?.reductionCO || '0'} toneladas / 25 anos
                        </h5>
                      </div>
                      <div className="col">
                        <p>
                          <FaTree />
                          Equivalente a árvores plantadas
                        </p>
                        <h5>
                          Aprox. {simulation?.tree || '0'} árvores / 25 anos
                        </h5>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="row">
                  <aside>
                    <IconPanelSolar />
                  </aside>

                  <div className="col">
                    <h5>Detalhes técnicos:</h5>
                    <p>
                      Enim fermentum consequat {simulation?.panel.min || '0'} a{' '}
                      {simulation?.panel.max || '0'} painéis tortor vulputate
                      sollicitudin et libero, suspendisse. Enim purus vitae a
                      nunc mattis sed pharetra natoque.
                    </p>
                  </div>
                </div>

                <div className="row">
                  <aside>
                    <hr />
                  </aside>
                  <div className="div-content">
                    <div className="div-infos">
                      <div className="col">
                        <p>
                          <FaBolt /> Potência:
                        </p>
                        <h5>
                          {simulation?.projectPotency.min || '0'} a{' '}
                          {simulation?.projectPotency.max || '0'}
                          kWp
                        </h5>
                      </div>
                      <div className="col">
                        <p>
                          <FaSolarPanel />
                          Energia gerada:
                        </p>
                        <h5>
                          {simulation?.generatedEnergy.min || '0'} a{' '}
                          {simulation?.generatedEnergy.max || '0'}
                          kWh/ano
                        </h5>
                      </div>
                      <div className="col">
                        <p>
                          <FaRulerCombined />
                          Área necessária:
                        </p>
                        <h5>
                          Entre {simulation?.areaRequired.min || '0'} a{' '}
                          {simulation?.areaRequired.max || '0'}m²
                        </h5>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </Card>
          </div>

          <div className="result-footer">
            <h2>Fale conosco</h2>

            <p>
              Cras volutpat feugiat mauris curabitur. Consectetur turpis
              tristique feugiat leo felis morbi. Integer sollicitudin auctor et
              faucibus eget tellus.
            </p>

            <div className="contact">
              <Link href={`https://wa.me/${company.whatsapp}`}>
                <a target="blank">
                  <FaWhatsapp size={24} />
                  <p>{phoneHandler(company.whatsapp)}</p>
                </a>
              </Link>

              <Link href={`tel:+${company.phone}`}>
                <a target="blank">
                  <FaPhoneAlt size={24} />
                  <p>{phoneHandler(company.phone)}</p>
                </a>
              </Link>

              <Link href={`mailto:${company.email}`}>
                <a target="blank">
                  <FaEnvelope size={24} />
                  <p>{company.email}</p>
                </a>
              </Link>
            </div>
          </div>

          <div className="result-copyright">
            <p>Simulação realizada por</p>
            <Image src="/assets/logo-copyright.png" height={31} width={135} />
          </div>
        </Content>
      </Container>
    </>
  );
};

export default Simulator;
// getStatic
export const getServerSideProps: GetServerSideProps = async ctx => {
  // const isAuth = getCookie('Aureos.isAuth', ctx);

  // if (!isAuth) {
  //   return {
  //     redirect: {
  //       destination: '/login',
  //       permanent: false,
  //     },
  //   };
  // }

  return {
    props: {},
  };
};

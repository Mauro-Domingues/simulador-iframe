import React from 'react';
import { ThemeProvider } from 'styled-components';

import { getCookie } from 'cookies-next';
import App from 'next/app';
import NextNprogress from 'nextjs-progressbar';

import AppProvider from '@/hooks';

import GlobalStyle from '@/styles/global';
import theme from '@/styles/theme';

const MyApp = ({ Component, pageProps }) => {
  return (
    <ThemeProvider theme={theme}>
      <NextNprogress
        color={theme.colors.yellow}
        startPosition={0.3}
        stopDelayMs={200}
        height={2}
      />

      <AppProvider>
        <Component {...pageProps} />
        <GlobalStyle />
      </AppProvider>
    </ThemeProvider>
  );
};

MyApp.getInitialProps = async appContext => {
  const appProps = await App.getInitialProps(appContext);

  const user = getCookie('Aureos.user', appContext.ctx);
  const token = getCookie('Aureos.token', appContext.ctx);

  return {
    ...appProps,
    dataAuth: {
      // keepConnected,
      user,
      token,
    },
  };
};

export default MyApp;

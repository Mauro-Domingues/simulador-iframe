export interface IContainer {
  children?: React.ReactNode;
  type?: 'auth';
}

export interface IContainerProps {
  type?: 'auth';
}

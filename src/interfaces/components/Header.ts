export interface IActiveLink {
  to?: string;
  textRoute: string;
  forceActive?: boolean;
  className?: string;
}

export interface IActiveLinkProps {
  active: boolean;
}

export interface IMenuProps {
  // menuOpened: boolean;
  absolute?: boolean;
}

export interface IHeaderProps {
  background?: string;
}

import { ButtonHTMLAttributes } from 'react';

export type IButtonOption = ButtonHTMLAttributes<HTMLButtonElement> & {
  active: boolean;
};

import { ButtonHTMLAttributes } from 'react';

export type IButtonPreview = ButtonHTMLAttributes<HTMLButtonElement> & {
  children: any;

  buttonBG?: string;
  buttonBGTransparent?: boolean;
  buttonBorderColor?: string;
  buttonBorderTransparent?: boolean;
  buttonTextColor?: string;
  radiusButton?: string;
  brilhoButton?: boolean;
};

export interface IContainerButtonPreview {
  buttonBG?: string;
  buttonBGTransparent?: boolean;
  buttonBorderColor?: string;
  buttonBorderTransparent?: boolean;
  buttonTextColor?: string;
  radiusButton?: string;
  brilhoButton?: boolean;
}

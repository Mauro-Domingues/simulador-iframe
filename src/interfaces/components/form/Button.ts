import { ButtonHTMLAttributes } from 'react';

export type IButton = ButtonHTMLAttributes<HTMLButtonElement> & {
  loading?: boolean;
  color?: string;
  background?: string;
  width?: number;

  typeButton: 'primary' | 'secondary' | 'ternary';
};

export interface IButtonProps {
  background?: string;
  color?: string;
  width?: number;
}

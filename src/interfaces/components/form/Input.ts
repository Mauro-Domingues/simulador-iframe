import { InputHTMLAttributes } from 'react';

export interface IInputProps extends InputHTMLAttributes<HTMLInputElement> {
  name: string;
  label: string;
  typeVariation?: 'primary' | 'secundary' | 'ternary';
  width?: number;
  children?: React.ReactNode;
}

export interface IContainerProps {
  isFocused?: boolean;
  isFilled?: boolean;
  isErrored?: boolean;
  width?: number;
  typeVariation?: 'primary' | 'secundary' | 'ternary';
}

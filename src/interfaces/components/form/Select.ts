import OptionTypeBase from 'react-select';

export interface ISelectProps {
  // export interface ISelectProps extends SelectProps<OptionTypeBase> {
  name: string;
  label: string;
  typeVariation?: 'primary' | 'secundary' | 'ternary';
  isMulti?: any;
  textNoOptions?: string;
  onChange?: any;

  options: IOptions[];
}

export interface IOptions {
  id?: number;
  value: string;
  label: string;
}

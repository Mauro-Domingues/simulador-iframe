import { Props as InputProps } from 'react-input-mask';

export interface IInputMaskProps extends InputProps {
  name: string;
  label: string;
}

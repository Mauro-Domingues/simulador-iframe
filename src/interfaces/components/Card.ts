export interface ICard {
  icon?: any;
  title?: string;
  borderColor?: string;
  backgroundColor?: string;
  children?: React.ReactNode;
}

export interface ICardContainer {
  borderColor?: string;
  backgroundColor?: string;
}

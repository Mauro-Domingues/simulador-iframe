import { ButtonHTMLAttributes } from 'react';

export type IButton = ButtonHTMLAttributes<HTMLButtonElement> & {
  color?: string;
  noLightReflection?: boolean;
};

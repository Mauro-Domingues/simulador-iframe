import React from 'react';

import { ICard } from '@/interfaces/components/Card';

import { Container } from './styles';

const Card: React.FC<ICard> = ({ icon, title, children, ...rest }) => {
  return (
    <Container className="card" {...rest}>
      {(icon || title) && (
        <div className="title">
          {icon} <h4>{title}</h4>
        </div>
      )}

      {children}
    </Container>
  );
};

export default Card;

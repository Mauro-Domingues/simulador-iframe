import styled, { css } from 'styled-components';

import { ICardContainer } from '@/interfaces/components/Card';

export const Container = styled.div<ICardContainer>`
  width: 100%;

  background: ${props =>
    props.backgroundColor ? props.backgroundColor : props.theme.colors.white};
  box-shadow: 0px 4px 32px 8px rgba(0, 0, 0, 0.05);
  border-radius: 1px;

  padding: 50px;

  > p {
    padding-right: 100px;
  }

  ${props =>
    props.borderColor &&
    css`
      border-left: 2px solid ${props.theme.colors[props.borderColor]};
    `}

  & + & {
    margin-top: 32px;
  }

  .title {
    display: flex;
    flex-direction: row;
    align-items: center;

    color: ${props => props.theme.colors.grayText3};

    margin-bottom: 30px;

    svg + h4 {
      margin-left: 16px;
    }
  }
`;

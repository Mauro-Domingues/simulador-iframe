import styled, { css } from 'styled-components';

interface IPropsInput {
  inputBG?: string;
  inputBGTransparent?: boolean;
  inputBorderColor?: string;
  inputBorderTransparent?: boolean;
  inputTextColor?: string;

  inputRadius?: string;
  inputBrilho?: boolean;

  isFocused?: boolean;
  isFilled?: boolean;
}

export const Container = styled.div<IPropsInput>`
  display: flex;
  align-items: center;
  position: relative;

  > div {
    position: absolute;
  }

  .preview {
    margin-right: 30px;

    height: 96px;
    width: 347px;

    background-color: ${props =>
      props.inputBGTransparent ? 'transparent' : props.inputBG};
    ${props =>
      props.inputBrilho &&
      css`
        box-shadow: 0px 4px 32px ${props.inputBorderColor}75;
      `};
  }

  > input {
    cursor: pointer;
    width: 685px;
    left: -202px;
    height: 96px;
    opacity: 0;
    position: absolute;
  }

  label {
    cursor: pointer;
    color: ${props => props.theme.colors.grayText};
  }

  > div > span {
    background-color: ${props => props.theme.colors.blueLight};
    color: ${props => props.theme.colors.white};

    &::before {
      border-color: ${props => props.theme.colors.blueLight} transparent;
    }
  }

  > div > div > span {
    width: 100% !important;
    height: 100% !important;
  }
`;

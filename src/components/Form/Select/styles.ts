import styled, { css } from 'styled-components';

import Tooltip from '@/components/Tooltip';

interface IPropsInput {
  width?: number;
  isErrored?: boolean;
  inputBG?: string;
  inputBGTransparent?: boolean;
  inputBorderColor?: string;
  inputBorderTransparent?: boolean;
  inputTextColor?: string;

  inputRadius?: string;
  inputBrilho?: boolean;

  isFocused?: boolean;
  isFilled?: boolean;
}

export const ContainerSelect = styled.div<IPropsInput>`
  display: flex;
  color: ${props => props.inputTextColor};
  font-size: 14px;

  .react-select__indicator,
  .react-select__control,
  .react-select__indicator-separator {
    border: none;
    color: unset;
    background-color: unset;
  }

  .react-select > {
    color: ${props => props.inputTextColor};
  }

  :hover .react-select__indicator {
    color: ${props =>
      props.inputBGTransparent ? '#000' : props.inputTextColor};
  }

  :hover .react-select__indicator-separator {
    background-color: ${props =>
      props.inputBGTransparent ? '#000' : props.inputTextColor};
  }

  .content-error {
    ${props =>
      props.isErrored &&
      css`
        position: relative !important;
      `}

    background-color: ${props =>
      props.inputBGTransparent ? '#fff' : props.inputBG};

    opacity: 1 !important;

    svg {
      opacity: 0;
    }
  }

  > div > div > div {
    height: 48px;
  }

  .react-select__control,
  .react-select__control:hover {
    height: 100%;
    background-color: ${props =>
      props.inputBGTransparent ? '#fff' : props.inputBG};
    ${props =>
      props.inputBrilho &&
      css`
        box-shadow: 0px 4px 32px ${props.inputBorderColor}75;
      `};
    color: ${props => props.inputTextColor};

    border: 1px ${props => props.inputBorderColor};
    border-style: ${props => (props.inputBorderTransparent ? 'none' : 'solid')};

    ${props =>
      props.isErrored &&
      css`
        border-color: #c53030;
      `}
  }

  .react-select__control--menu-is-open {
    border-radius: ${props => `${props.inputRadius}px`}
      ${props => `${props.inputRadius}px`} 0 0 !important;
    padding-left: calc(${props => `${props.inputRadius}px`} / 2.5) !important;
    padding-right: calc(${props => `${props.inputRadius}px`} / 2.5) !important;
  }

  #react-select-long-value-select-listbox {
    border-radius: 0 0 ${props => `${props.inputRadius}px`}
      ${props => `${props.inputRadius}px`} !important;
    border-style: ${props =>
      !props.inputBorderTransparent ? 'none' : 'solid'};
    border: ${props =>
      props.inputBGTransparent
        ? 'none !important'
        : `0.5px ${props.inputTextColor}`};

    margin-top: 0;
    display: flex important;
    overflow: hidden;

    > div {
      border: 1px ${props => props.inputBorderColor};
      border-style: ${props =>
        props.inputBorderTransparent ? 'none' : 'solid'};
      border-radius: 0 0 ${props => `${props.inputRadius}px`}
        ${props => `${props.inputRadius}px`} !important;
      background-color: ${props =>
        props.inputBGTransparent ? '#fff' : props.inputBG};
      border-top: none;
      > div {
        padding-left: calc(
          ${props => `${props.inputRadius}px`} / 2.4
        ) !important;
        padding-left: min(10px) !important;
        background-color: ${props =>
          props.inputBGTransparent ? '#fff' : props.inputBG};
        text-shadow: ${props =>
          !props.inputBGTransparent ? '0 0 1px #555' : 'none'};
        :hover {
          color: ${props =>
            !props.inputBGTransparent
              ? props.inputBG
              : props.inputTextColor} !important;
          background-color: ${props =>
            !props.inputBGTransparent
              ? props.inputTextColor
              : props.inputBG} !important;
        }
      }
      > div:last-child {
        border-radius: 0 0 ${props => `${props.inputRadius}px`}
          ${props => `${props.inputRadius}px`} !important;
      }
    }

    .react-select__value-container {
      display: flex;
      height: 100%;
      padding-left: 13px;

      .react-select__placeholder {
        color: ${props =>
          props.inputBGTransparent ? '#fff' : props.inputTextColor};
      }

      .react-select__input-container {
        height: 100%;

        margin: 0 !important;
        padding: 0 !important;
      }
    }

    .react-select__menu {
      background: ${props =>
        !props.inputBGTransparent ? '#fff' : props.inputBG};
      border-radius: ${props => props.inputRadius};
      z-index: 5;
      > div > div {
        display: flex;
      }
    }

    .react-select__menu > div > div:hover {
      background-color: ${props =>
        props.inputBGTransparent ? '#fff' : props.inputBG};
      color: ${props =>
        !props.inputBGTransparent ? '#fff' : props.inputTextColor};
    }
  }
`;

export const Container = styled.div<IPropsInput>`
  width: 100%;
  margin-bottom: 16px;

  display: flex;
  flex-direction: column;

  > label {
    font-weight: 500;
    font-size: 12px;
    line-height: 18px;
    text-transform: capitalize;
    color: ${props => props.inputTextColor};

    margin-bottom: 4px;
  }

  > input {
    width: 100%;
    height: 46px !important;

    padding: 12px 16px;

    color: ${props => props.inputTextColor};

    background-color: ${props =>
      props.inputBGTransparent ? 'transparent' : props.inputBG};

    border: 1px ${props => props.inputBorderColor};
    border-style: ${props => (props.inputBorderTransparent ? 'none' : 'solid')};

    border-radius: ${props => `${props.inputRadius}px`};

    box-shadow: ${props =>
      props.inputBrilho ? `0px 4px 32px ${props.inputBorderColor}75` : 'none'};

    ::placeholder {
      color: ${props => props.inputTextColor};
    }

    :focus {
      outline: none;
    }
  }
`;

export const Error = styled(Tooltip)`
  height: 100%;

  display: flex;
  align-items: center;
  position: absolute;
  margin-right: -10px;

  svg {
    margin: 0;
  }

  span {
    bottom: 50px;
    min-width: 95px;

    background: #c53030;
    color: #fff;

    &::before {
      border-color: #c53030 transparent;
    }
  }
`;

/* eslint-disable react/require-default-props */
import React, { useRef, useEffect, InputHTMLAttributes } from 'react';
import { FiAlertCircle } from 'react-icons/fi';
import ReactSelect from 'react-select';

import { useField } from '@unform/core';

import { ISelectProps } from '@/interfaces/components/form/Select';

import { Error, Container, ContainerSelect } from './styles';

export interface IOptions {
  id?: number;
  value: string;
  label: string;
}

interface IInputProps extends InputHTMLAttributes<HTMLInputElement> {
  name: string;
  width?: number;
  label: string;
  textNoOptions?: string;
  isMulti?: any;
  options?: IOptions[];
  onChange?: any;
  ref?: any;
  inputBG?: string;
  inputBGTransparent?: boolean;
  inputBorderColor?: string;
  inputBorderTransparent?: boolean;
  inputTextColor?: string;
  inputRadius?: string;
  inputBrilho?: boolean;
}

const Select: React.FC<IInputProps> = ({
  name,
  isMulti,
  options,
  label,
  textNoOptions,
  ...rest
}) => {
  const selectRef = useRef(null);
  const { fieldName, defaultValue, registerField, error } = useField(name);

  useEffect(() => {
    registerField({
      name: fieldName,
      ref: selectRef.current,
      getValue: (ref: any) => {
        if (isMulti) {
          if (!ref.state.selectValue) {
            return [];
          }
          return ref.state.selectValue.map((option: any) => option.value);
        }

        if (!ref.state.selectValue) {
          return '';
        }

        if (!ref.state.selectValue[0]) {
          return '';
        }

        return ref.state.selectValue[0].value;
      },
      clearValue: (ref: any) => {
        ref.select.clearValue();
      },
      setValue: (ref: any, value: any) => {
        ref.select.setValue(value);
      },
    });
  }, [fieldName, registerField, isMulti]);

  return (
    <ContainerSelect {...rest}>
      <Container
        isErrored={!!error}
        isFocused={selectRef.current?.value}
        isFilled={selectRef.current?.value}
      >
        <div className="Content">
          <label htmlFor={name}>{label}</label>

          <ReactSelect
            ref={selectRef}
            defaultValue={defaultValue}
            noOptionsMessage={() => textNoOptions}
            isMulti={isMulti}
            options={options}
            classNamePrefix="react-select"
            id="long-value-select"
            instanceId="long-value-select"
            {...rest}
            theme={theme => ({
              ...theme,
              borderRadius: Number(rest.inputRadius),
              colors: {
                ...theme.colors,
                danger: 'unset',
                dangerLight: 'unset',
                primary: 'unset', // Background do item e da borda do select quando selecionado
                primary25: 'unset', // background dos itens do select sob efeito do hover
                primary50: 'unset', // background dos itens do select quando clicado
                primary75: 'unset',
                neutral0: 'unset', // Background da lista de itens
                neutral5: 'unset',
                neutral10: 'unset',
                neutral20: 'unset', // Borda do select
                neutral30: 'unset', // Borda do select no hover
                neutral40: 'unset', // Borda da seta do select no hover
                neutral50: 'unset',
                neutral60: 'unset', // Cor da seta do input quando ele esta selecionado
                neutral70: 'unset',
                neutral80: 'unset', // cor da fonte do input
                neutral90: 'unset',
              },
            })}
            {...rest}
          />
        </div>

        <Error title={error} className="content-error">
          <FiAlertCircle color="#c53030" size={20} />
        </Error>
      </Container>
    </ContainerSelect>
  );
};

export default Select;

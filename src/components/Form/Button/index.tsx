import React from 'react';

import { IButton } from '@/interfaces/components/form/Button';

import Loading from '@/components/Loading';

import { Container } from './styles';

const Button: React.FC<IButton> = ({ loading, children, ...rest }) => {
  return (
    <Container disabled={loading} {...rest}>
      <>
        {loading && <Loading size={30} />}

        {!loading && children}
      </>
    </Container>
  );
};

export default Button;

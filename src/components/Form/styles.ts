import styled, { css } from 'styled-components';

import { IContainerProps } from '@/interfaces/components/form/Input';

import Tooltip from '@/components/Tooltip';

const colorsTypeVariationsActive = {
  primary: css`
    color: ${props => props.theme.colors.grayText};
  `,
  secundary: css`
    color: ${props => props.theme.colors.blueLight3};
  `,
  ternary: css`
    color: ${props => props.theme.colors.blueLight3};
  `,
};

const colorsTypeVariations = {
  primary: css`
    color: ${props => props.theme.colors.blueLight3};
  `,
  secundary: css`
    color: ${props => props.theme.colors.grayText};
  `,
  ternary: css`
    color: ${props => props.theme.colors.grayText};
  `,
};

const colorsTypeVariationsPlaceholder = {
  primary: css`
    color: ${props => props.theme.colors.blueLight3};
  `,
  secundary: css`
    color: ${props => props.theme.colors.grayText};
  `,
  ternary: css`
    color: ${props => props.theme.colors.grayText4};
  `,
};

export const Container = styled.div<IContainerProps>`
  width: ${props => (props.width ? `${props.width}%` : '100%')};

  display: flex;

  margin-bottom: 15px;

  .Content {
    display: flex;
    flex-direction: column;

    width: 100%;

    position: relative;

    label {
      ${props => colorsTypeVariations[props.typeVariation || 'primary']}

      font-weight: 500;
      font-size: 12px;
      line-height: 18px;

      transition: color 0.8s;

      ${props =>
        props.isFocused &&
        css`
          ${colorsTypeVariationsActive[props.typeVariation || 'primary']}
        `}
    }

    input,
    textarea {
      width: 100%;
      height: 48px;
      padding: 0 16px 0;
      background: ${props => props.theme.colors.white};
      box-shadow: 0px 4px 56px 4px rgba(0, 0, 0, 0.08);
      border: none;

      display: flex;
      align-items: center;

      /* ${props =>
        props.isErrored &&
        css`
          border-color: #c53030;
        `} */

      color: ${props => props.theme.colors.grayText};

      font-family: 'Poppins', sans-serif;
      font-style: normal;
      font-weight: 400;
      font-size: 16px;
      line-height: 24px;

      :focus {
        outline: none;
      }

      ::placeholder {
        ${props =>
          colorsTypeVariationsPlaceholder[props.typeVariation || 'primary']}

        transition: color 0.8s;

        ${props =>
          props.isFocused &&
          css`
            color: ${props.theme.colors.grayText};
          `}
      }
    }
  }

  button {
    background-color: transparent;
    border: none;
    width: 35px;
    margin-top: 18px;
    height: 48px;

    position: absolute;
    right: 0;

    color: ${props => props.theme.colors.grayText};
  }

  position: relative;

  > div:last-child {
    opacity: 0;
    margin-top: 18px;
    height: 0;

    position: absolute;
    right: 0;

    background: ${props => props.theme.colors.white};

    transition: opacity 0.5s;
    transition: height 0.01s;

    ${props =>
      props.isErrored &&
      css`
        opacity: 1;

        height: 48px;

        svg {
          opacity: 1;
        }
      `}
  }
`;

export const Error = styled(Tooltip)`
  height: 100%;

  display: flex;
  align-items: center;
  position: absolute;
  margin-right: -10px;

  svg {
    margin: 0;
  }

  span {
    bottom: 50px;
    min-width: 95px;

    background: #c53030;
    color: #fff;

    &::before {
      border-color: #c53030 transparent;
    }
  }
`;

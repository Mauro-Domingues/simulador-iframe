import styled, { css } from 'styled-components';

import Tooltip from '@/components/Tooltip';

interface IPropsInput {
  inputBG?: string;
  inputBGTransparent?: boolean;
  inputBorderColor?: string;
  inputBorderTransparent?: boolean;
  inputTextColor?: string;

  inputRadius?: string;
  inputBrilho?: boolean;

  isFocused?: boolean;
  isFilled?: boolean;
}

export const Container = styled.div<IPropsInput>`
  width: 100%;
  margin-bottom: 16px;

  display: flex;
  flex-direction: column;

  > label {
    font-weight: 500;
    font-size: 12px;
    line-height: 18px;
    text-transform: capitalize;
    color: ${props => props.inputTextColor};

    margin-bottom: 4px;
  }

  > input {
    width: 100%;
    height: 46px !important;

    padding: 12px 16px;

    color: ${props => props.inputTextColor};

    background-color: ${props =>
      props.inputBGTransparent ? 'transparent' : props.inputBG};

    border: 1px ${props => props.inputBorderColor};
    border-style: ${props => (props.inputBorderTransparent ? 'none' : 'solid')};

    border-radius: ${props => `${props.inputRadius}px`};

    ${props =>
      props.inputBrilho &&
      css`
        box-shadow: 0px 4px 32px ${props.inputBorderColor}75;
      `};

    ::placeholder {
      color: ${props => props.inputTextColor};
    }

    :focus {
      outline: none;
    }
  }
`;

export const Error = styled(Tooltip)`
  height: 100%;

  display: flex;
  align-items: center;
  position: absolute;
  margin-right: -10px;

  svg {
    margin: 0;
  }

  span {
    bottom: 50px;
    min-width: 95px;

    background: #c53030;
    color: #fff;

    &::before {
      border-color: #c53030 transparent;
    }
  }
`;

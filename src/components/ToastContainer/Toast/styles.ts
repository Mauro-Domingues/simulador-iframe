import { animated } from 'react-spring';
import styled, { css } from 'styled-components';

import theme from '@/styles/theme';

interface ToastProps {
  type?: 'success' | 'error' | 'info';
  $hasDescription: boolean;
  color?: string;
}

export const Container = styled(animated.div)<ToastProps>`
  width: 360px;

  position: relative;

  display: flex;

  box-shadow: 2px 2px 8px rgba(0, 0, 0, 0.2);

  border: 1px solid ${props => props.theme.colors.blueLight};

  margin: 30px 30px 0 40px;

  & + div {
    margin-top: 10px !important;
  }

  &:last-child {
    margin-bottom: 30px !important;
  }

  background-color: ${props => props.theme.colors.blueLight4};

  .content {
    margin-left: 20px;
    flex: 1;
    padding: 16px 40px 16px 0;
    display: flex;

    color: ${props => props.theme.colors.yellow};

    > svg {
      margin: 4px 12px 0 0;
    }
  }

  .text {
    flex: 1;

    p {
      margin-top: 4px;
      font-size: 14px;
      opacity: 0.8;
      line-height: 20px;
    }
  }

  button {
    position: absolute;
    right: 16px;
    top: 19px;
    border: 0;
    background: transparent;
    color: inherit;

    color: ${props => props.theme.colors.yellow};
  }

  ${props =>
    !props.$hasDescription &&
    css`
      align-items: center;

      svg {
        margin-top: 0;
      }
    `}

  > hr {
    width: 9px;

    border: 0;
    background-color: ${props => props.color || theme.colors.yellow3};

    opacity: 1;

    transition: all 100ms linear;

    box-shadow: inset 0px 0px 0.5em 0
        ${props => props.color || theme.colors.blueLight3},
      0px 0px 0.5em 0 ${props => props.color || theme.colors.blueLight3};

    ::after {
      content: '';
      width: 9px;
      position: absolute;
      inset: 0;
      background-color: transparent;

      box-shadow: 0 0 2em 0.2em ${props => props.color || theme.colors.grayText};

      z-index: -1;
    }
  }
`;

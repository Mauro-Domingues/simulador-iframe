import React, { useEffect } from 'react';
import {
  FiAlertCircle,
  FiXCircle,
  FiCheckCircle,
  FiInfo,
} from 'react-icons/fi';

import { ToastMessage, useToast } from '@/hooks/toast';

import { Container } from './styles';

interface ToastProps {
  message: ToastMessage;
  style: any;
}

const icons = {
  info: <FiInfo size={24} />,
  error: <FiAlertCircle size={24} />,
  success: <FiCheckCircle size={24} />,
};

const colors = {
  info: '#0E76A8',
  error: '#c53030',
  success: '#25D366',
};

const Toast: React.FC<ToastProps> = ({ message, style }) => {
  const { removeToast } = useToast();

  useEffect(() => {
    const timer = setTimeout(() => {
      removeToast(message.id);
    }, 3000);

    return () => {
      clearTimeout(timer);
    };
  }, [removeToast, message.id]);

  return (
    <Container
      $hasDescription={!!message.description}
      type={message.type}
      style={style}
      color={colors[message.type || 'info']}
    >
      <hr />
      <div className="content">
        {icons[message.type || 'info']}
        <div className="text">
          <strong>{message.title}</strong>
          {message.description && <p>{message.description}</p>}
        </div>
      </div>

      <button onClick={() => removeToast(message.id)} type="button">
        <FiXCircle size={18} />
      </button>
    </Container>
  );
};

export default Toast;

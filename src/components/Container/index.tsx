import React from 'react';

import { IContainer } from '@/interfaces/components/Container';

import { Main } from './styles';

const Container: React.FC<IContainer> = ({ children }) => {
  return <Main>{children}</Main>;
};

export default Container;

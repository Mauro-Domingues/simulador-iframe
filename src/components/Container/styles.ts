import styled, { css } from 'styled-components';

import { IContainerProps } from '@/interfaces/components/Container';

import theme from '@/styles/theme';

export const Main = styled.main<IContainerProps>`
  width: 100%;
  min-height: 100vh;

  display: flex;
  justify-content: center;

  background: ${theme.colors.background};

  main {
    argin: 0;
  }

  section {
    display: flex;
    //flex-direction: row;

    width: 100%;
    min-height: 100vh;

    position: relative;

    .scroll {
      height: calc(100vh - 120px);
      overflow-y: auto;

      padding: 40px 80px;
    }

    hr {
      width: 80px;
      height: 3px;
      border: none;
      background: linear-gradient(90deg, #ffd04f 0%, #54b7f3 100%);
    }

    a {
      font-weight: 500;
      font-size: 16px;
      line-height: 24px;
      /* identical to box height */

      text-align: right;
      text-decoration-line: underline;

      color: #7f8487;
    }
  }
`;

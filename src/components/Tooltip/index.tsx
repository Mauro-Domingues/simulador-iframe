import React from 'react';

import { ITooltip } from '@/interfaces/components/Tooltip';

import { Container } from './styles';

const Tooltip: React.FC<ITooltip> = ({ title, className, children }) => (
  <Container className={className}>
    {children}
    <span>{title}</span>
  </Container>
);

export default Tooltip;

import styled, { css } from 'styled-components';

import { IContainerButtonPreview } from '@/interfaces/components/previews/Button';

export const Container = styled.button<IContainerButtonPreview>`
  width: 196px;
  height: 46px;

  background-color: ${props =>
    props.buttonBGTransparent ? 'transparent' : props.buttonBG};
  border: 1px ${props => props.buttonBorderColor};
  border-style: ${props => (props.buttonBorderTransparent ? 'none' : 'solid')};
  border-radius: ${props => `${props.radiusButton}px`};

  color: ${props => props.buttonTextColor};

  ${props =>
    props.brilhoButton &&
    css`
      box-shadow: 0px 4px 32px ${props.buttonBG}75;
    `};
`;

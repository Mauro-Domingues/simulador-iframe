import React from 'react';

import { IButtonPreview } from '@/interfaces/components/previews/Button';

import { Container } from './styles';

const Button: React.FC<IButtonPreview> = ({ children, ...rest }) => {
  return (
    <Container className="button-preview" {...rest}>
      {children}
    </Container>
  );
};

export default Button;

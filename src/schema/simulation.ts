import * as Yup from 'yup';

export const simulationSchema = Yup.object().shape({
  name: Yup.string().required('Insira seu nome'),
  email: Yup.string()
    .email('O email é obrigatório!')
    .required('O email é obrigatório!'),
  phone: Yup.string()
    .required('Insira um número de contato')
    .matches(/^(\d{12,13})?$/, 'O número está incompleto'),
  segment_id: Yup.string().required('É necessário informar o seu segmento'),
  state_id: Yup.string().required('É necessário informar o seu estado'),
  city_id: Yup.string().required('É necessário informar a sua cidade'),
  value: Yup.string().required('É necessário informar o valor da sua conta'),
});

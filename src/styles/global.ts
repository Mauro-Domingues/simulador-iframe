import { createGlobalStyle } from 'styled-components';

export default createGlobalStyle`

  html{
    min-height: 1920px;
    max-width: 787px;
  }

  * {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
  }

  ::selection {
      //background: #2d313a;
      background-color:#114F73;
      color: #E4E4E4;
  }


  ::-webkit-scrollbar{
    width: 6px;
  }

  ::-webkit-scrollbar-track{
    background-color: #E4E4E4;
    border-radius: 19px;
  }

  ::-webkit-scrollbar-thumb {
    background-color: #C4C4C4;
    border-radius: 19px;
  }

  body {
    background: ${props => props.theme.colors.background};
    font-family: 'Poppins', sans-serif;
    font-weight: 400;
  }

  body::-webkit-scrollbar {
    display: none;
  }

  a {
    text-decoration: none;
  }

  a, button {
    cursor: pointer;
  }

  input, textarea, h1, h2, h3, h4, h5, p, li, a, button, label {
    font-family: 'Poppins', sans-serif;
    font-style: normal;
    font-weight: normal;
  }

  h2{
    font-weight: 300;
    font-size: 32px;
    line-height: 48px;
    color: ${props => props.theme.colors.grayText3};
  }

  h3{
    font-weight: 600;
    font-size: 24px;
    line-height: 36px;
    color: ${props => props.theme.colors.blueText};
  }

  h4{
    font-weight: 600;
    font-size: 18px;
    line-height: 27px;
    color: ${props => props.theme.colors.grayText3};
  }

  h5{
    font-weight: 600;
    font-size: 18px;
    line-height: 27px;
    color: ${props => props.theme.colors.blueText};
  }

  p{
    font-weight: 400;
    font-size: 14px;
    line-height: 21px;
    color: ${props => props.theme.colors.grayText2};
  }
`;

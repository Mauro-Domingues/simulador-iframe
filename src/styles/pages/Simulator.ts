import styled, { css } from 'styled-components';

interface IProps {
  buttonBG?: string;
  buttonBGTransparent?: boolean;
  buttonBorderColor?: string;
  buttonBorderTransparent?: boolean;
  buttonTextColor?: string;
  radiusButton?: string;
  brilhoButton?: boolean;

  inputBG?: string;
  inputBGTransparent?: boolean;
  inputBorderColor?: string;
  inputBorderTransparent?: boolean;
  inputTextColor?: string;

  inputRadius?: string;
  inputBrilho?: boolean;

  isActive: 'form' | 'result';

  primaryColor?: string;
  secondaryColor?: string;
  tertiaryColor?: string;
  titleColor?: string;

  logoURL?: string;
}

interface IPropsHeaderCard {
  isActive: 'form' | 'result';
}

export const HeaderCard = styled.div<IPropsHeaderCard>`
  width: 100%;

  padding: 19px 0 0 44px;

  background: #f9f9f9;
  border-radius: 1px;

  z-index: 2;

  > button {
    flex: 1;

    padding: 12px 5px;
    margin: 0 15px;

    border: none;
    background-color: transparent;

    border-bottom: solid transparent 1px;
    color: #9f9f9f;
    font-weight: 600;
    font-size: 14px;
    line-height: 21px;

    transition: all 0.5s;

    :first-child {
      ${props =>
        props.isActive === 'form' &&
        css`
          color: #24a5f2;
          border-bottom: solid #5cbaf2 1px;
        `}
    }
    :last-child {
      ${props =>
        props.isActive === 'result' &&
        css`
          color: #24a5f2;
          border-bottom: solid #5cbaf2 1px;
        `}
    }
  }
`;

export const Content = styled.section<IProps>`
  background-color: transparent;
  display: flex;
  flex-direction: column;

  form {
    padding: 32px 0 0;

    .row {
      display: flex;
      flex-direction: row;
      gap: 30px;

      justify-content: space-between;

      > div {
        width: 100%;
      }
    }

    > label {
      font-weight: 500;
      font-size: 12px;
      line-height: 18px;
      text-transform: capitalize;
      color: ${props => props.inputTextColor};

      margin-bottom: 4px;
    }

    .input-file {
      display: flex;
      align-items: center;
      justify-content: center;

      width: 100%;
      height: 96px;

      margin-bottom: 48px;

      color: ${props => props.inputTextColor};

      background: ${props =>
        props.inputBGTransparent ? 'transparent' : props.inputBG};

      border: 1px ${props => props.inputBorderColor};
      border-style: ${props =>
        props.inputBorderTransparent ? 'none' : 'solid'};

      border-radius: ${props => `${props.inputRadius}px`};

      ${props =>
        props.inputBrilho &&
        css`
          box-shadow: 0px 4px 32px ${props.inputBorderColor}75;
        `};
    }

    strong {
      color: ${props => `${props.titleColor}`};
    }

    > button {
      max-width: 196px;
    }
  }

  .cards {
    display: flex;
    box-shadow: inset 4px 4px 80px rgba(0, 0, 0, 0.5);
    transform-style: preserve-3d;
    position: relative;
    transition: transform 0.5s;
    perspective: 1000px;

    ${props =>
      props.isActive === 'result' &&
      css`
        transform: rotateY(180deg);
      `}
  }

  .card {
    max-height: 10000px;

    :first-child {
      position: absolute;
      width: 100%;
      backface-visibility: hidden;
    }

    :last-child {
      margin-top: 0;
      backface-visibility: hidden;
      position: absolute;
      width: 100%;

      transform: rotateY(180deg);
    }
  }

  .result-header {
    display: flex;
    flex-direction: row;
    justify-content: space-between;

    > div {
      display: flex;
      flex-direction: column;
    }

    .div-logo {
      > p {
        font-weight: 600;
        font-size: 18px;
        line-height: 27px;
        color: ${props => props.titleColor};
      }
    }

    .div-date {
      justify-content: flex-end;

      > p {
        font-weight: 400;
        font-size: 14px;
        line-height: 21px;

        color: ${props => props.theme.colors.grayText};
      }
    }

    .div-data {
      justify-content: space-between;

      .buttons {
        > button {
          display: flex;
          flex-direction: row;
          align-items: center;

          border: none;
          background-color: transparent;
          color: ${props => props.theme.colors.blueLight4};

          > p {
            padding: 5px 0 5px 7px;

            font-weight: 400;
            font-size: 14px;
            line-height: 21px;
            text-decoration-line: underline;

            color: ${props => props.theme.colors.blueLight4};
          }
        }
      }

      .ref {
        > p {
          font-weight: 400;
          font-size: 14px;
          line-height: 21px;
          color: ${props => props.theme.colors.grayText};
        }

        > span {
          font-weight: 400;
          font-size: 18px;
          line-height: 27px;

          color: ${props => props.titleColor};
        }
      }
    }
  }

  .line {
    width: 100%;
    height: 1px;
    margin: 40px 0;

    background: #e6e6e6;
  }

  > h3 {
    color: ${props => props.titleColor};
  }

  .result {
    width: 100%;

    > h2 {
      color: ${props => props.titleColor};
    }

    > div {
      padding: 40px 0 0;

      :first-child {
        padding-top: 16px;
      }
    }

    .col {
      display: flex;
      flex-direction: column;

      > p {
        font-weight: 400;
        font-size: 14px;
        line-height: 21px;

        color: ${props => props.theme.colors.grayText};
      }

      > span {
        font-weight: 400;
        font-size: 18px;
        line-height: 27px;

        color: ${props => props.titleColor};
      }
    }

    .row {
      display: flex;
      flex-direction: row;

      width: 100%;

      > aside {
        width: 26.5%;

        display: flex;
        align-items: center;
        justify-content: center;

        svg {
          color: ${props => props.tertiaryColor} !important;
          path {
            fill: ${props => props.tertiaryColor} !important;
          }
        }

        > hr {
          height: 114px;
          width: 1px;

          background: ${props => `${props.tertiaryColor}`};

          opacity: 0.5;
        }
      }

      > div {
        width: 73.5%;
      }
    }

    .applicant-data {
      display: flex;
      flex-direction: row;
      justify-content: space-between;

      padding: 18px 0 24px;

      border-bottom: 1px solid #e6e6e6;
    }

    h5 {
      color: ${props => props.titleColor};
    }

    .div-content {
      display: flex;
      align-items: center;

      .card-value {
        background-color: ${props => props.secondaryColor}10;

        width: min-content;
        padding: 8px 16px;

        > p {
          font-weight: 400;
          font-size: 16px;
          line-height: 24px;
          color: #727272;

          width: max-content;

          > strong {
            color: ${props => props.secondaryColor};
          }
        }

        > h2 {
          color: #1e4a8a;
        }
      }
    }

    .div-infos {
      width: 100%;
      display: flex;
      flex-direction: row;
      justify-content: space-between;

      svg {
        margin-right: 7px;
      }
    }
  }

  .result-footer {
    background-color: ${props => `${props.primaryColor}`};
    transform: translateY(1435px);
    padding: 0;
    width: 0px;

    overflow: hidden;
    transition: width 0.5s, padding 0.5s;

    max-height: 0;

    ${props =>
      props.isActive === 'result' &&
      css`
        width: 100%;
        padding: 56px 64px;

        max-height: 1000px;
      `}

    h2 {
      color: #fafafa;
      margin-bottom: 24px;
    }

    > p {
      margin-bottom: 40px;

      max-width: 535px;
    }

    .contact {
      width: 100%;
      max-width: 535px;

      display: flex;
      flex-direction: row;
      flex-wrap: wrap;

      a {
        display: flex;
        flex-direction: row;

        margin-right: 60px;
        margin-bottom: 25px;

        text-decoration-line: none;

        svg {
          color: ${props => props.tertiaryColor};
        }

        > p {
          margin-left: 12px;

          font-weight: 500;
          font-size: 16px;
          line-height: 24px;
          color: #e6e6e6;
        }

        :hover {
          > p {
            color: ${props => props.tertiaryColor};
          }
        }
      }
    }
  }

  .result-copyright {
    background-color: #fafafa;
    transform: translateY(1435px);

    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: center;

    padding: 0;
    width: 0px;

    overflow: hidden;
    transition: width 0.5s, padding 0.5s;

    max-height: 0;

    ${props =>
      props.isActive === 'result' &&
      css`
        width: 100%;
        padding: 15px 0 10px 0;

        max-height: 1000px;
      `}

    p {
      padding-right: 16px;
      font-weight: 400;
      font-size: 12px;
      line-height: 18px;

      color: ${props => `${props.titleColor}`};
    }
  }
`;

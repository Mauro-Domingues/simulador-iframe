const theme = {
  colors: {
    background: '#F5F5F5',

    yellow: '#FFD04F',
    yellow2: '#FFDA77',
    yellow3: '#DDA200',
    white: '#ffffff',
    blue: '#114F73',
    blue1: '#104067',
    blueText: '#083F74',
    grayText: '#878787',
    grayText2: '#9F9F9F',
    grayText3: '#53606D',
    grayText4: '#6D6D6D',
    placeholder: '#C4C4C4',
    blueLight: '#5CBAF2',
    blueLight2: '#06D8FF',
    blueLight3: '#24A5F2',
    blueLight4: '#3582B5',

    orange: '#F07D18',

    yellowBorder: '#FFBB00',

    redCancel: '#FF4F4F',
  },
};

export default theme;

export default function phoneHandler(phone_number: string): string {
  if (phone_number) {
    if (phone_number.length <= 12) {
      const formatedNumber = `+${phone_number?.slice(
        0,
        2,
      )} (${phone_number?.slice(2, 4)}) ${phone_number?.slice(
        4,
        8,
      )} - ${phone_number?.slice(8)}`;
      return formatedNumber;
    }
    const formatedNumber = `+${phone_number?.slice(
      0,
      2,
    )} (${phone_number?.slice(2, 4)}) ${phone_number?.slice(
      4,
      9,
    )} - ${phone_number?.slice(9)}`;

    return formatedNumber;
  }
  return null;
}

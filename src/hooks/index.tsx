import React from 'react';

import { ToastProvider, IToastProvider } from './toast';

const AppProvider: React.FC<IToastProvider> = ({ children }) => (
  <ToastProvider>{children}</ToastProvider>
);

export default AppProvider;

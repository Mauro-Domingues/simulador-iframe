/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  eslint: {
    ignoreDuringBuilds: true,
  },

  compiler:{
    styledComponents: true,
  },
  images: {
    domains: ['api.aureos.risestudio.com.br', 'localhost'],
    formats: ['image/avif', 'image/webp'],
  },

  webpack(config) {
    config.module.rules.push({
      test: /\.svg$/,

      use: ["@svgr/webpack"]
    });

    return config;
  }


}

module.exports = nextConfig
